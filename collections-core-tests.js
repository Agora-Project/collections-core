// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by collections-core.js.
import { name as packageName } from "meteor/agoraforum:collections-core";

// Write your tests here!
// Here is an example.
Tinytest.add('collections-core - example', function (test) {
  test.equal(packageName, "collections-core");
});
