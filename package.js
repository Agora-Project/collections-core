Package.describe({
    name: 'agoraforum:collections-core',
    version: '0.1.5',

    summary: 'Collections for the Agora forum software.',

    git: 'https://gitlab.com/Agora-Project/collections-core',

    documentation: null,
    //documentation: 'README.md',
    license: 'LICENSE'
});

Npm.depends({
    'node-rsa': '1.0.0'
});

Package.onUse(function(api) {
    api.versionsFrom('1.8.0.2');
    api.use([
        'ecmascript',
        'mongo',
        'accounts-password',
        'matb33:collection-hooks@0.8.4',
        'agoraforum:activitypub@0.0.3',
        'alanning:roles@1.2.16',
    ]);

    api.use([
        'percolate:migrations@1.0.2',
    ], 'server');

    api.addFiles([
        'lib/actors.js',
        'lib/posts.js',
        'lib/users.js',
        'lib/orderedCollections.js',
        'lib/activities.js'
    ]);

    api.addFiles([
        'lib/keys.js',
        'lib/publish.js',
        'lib/migrations.js',
    ], 'server');
});

Package.onTest(function(api) {
    api.use('ecmascript');
    api.use('tinytest');
    api.use('agoraforum:collections-core');
    api.mainModule('collections-core-tests.js');
});
