/*
    Agora Forum Software
    Copyright (C) 2016 Gregory Sartucci
    License: AGPL-3.0, Check file LICENSE
*/

//Returns all information about a single post and its poster's basic information.
Meteor.publish('fullPost', function(postID) {

    let post = Posts.find({id: postID}, {fields: {local: false} })

    let postData = post.fetch()[0];

    let subscription = [
        post,
        Actors.find({id: postData.attributedTo}),
        ReplyLists.find({id: postData.replies}),
        LikeLists.find({id: postData.likes}),
        ShareLists.find({id: postData.shares})
    ];

    if (this.userId) {
        let actorID = Meteor.users.findOne({_id: this.userId}).actor;
        subscription.push(Activities.find({actor: actorID, type: {$in: ["Like", "Announce"]}, object: postID}));
    }

    return subscription;
});

Meteor.publish('abstractPost', function(postID) {
    return Posts.find({id: postID}, {fields: {id: true, published: true, attributedTo: true, inReplyTo: true, replies: true} } );
});

Meteor.publish('abstractPostAndReplies', function(postID) {
    return Posts.find({$or: [{id: postID}, {inReplyTo: postID}]}, {fields: {id: true, published: true, attributedTo: true, inReplyTo: true, replies: true} } );
});

Meteor.publish('abstractReplies', function(postID) {
    return Posts.find({inReplyTo: postID}, {fields: {id: true, published: true, attributedTo: true, inReplyTo: true, replies: true} } );
});

let manageLimit = function(limit) {
    if (limit > 10000) limit = 10000;
    return limit;
}

//Returns an abstract shell of all posts, each only containing its id, links, and subtree width.
Meteor.publish('localAbstractPosts', function(limit = 1000, date = Date.now()) {
    limit = manageLimit(limit);
    if (date)
        return Posts.find({local: true, published:{ $lte: date}}, {fields: {id: true, published: true, attributedTo: true, inReplyTo: true, replies: true}, sort: {published: 1}, limit: limit});
    else
        return Posts.find({local: true}, {fields: {id: true, published: true, attributedTo: true, inReplyTo: true, replies: true}, sort: {published: 1}, limit: limit});
});

//Returns an abstract shell of all posts after the given date, each only containing its id, links, and subtree width.
Meteor.publish('abstractPosts', function(limit = 1000, date = Date.now()) {
    limit = manageLimit(limit);
    if (date)
        return Posts.find({published:{ $lte: date}}, {fields: {id: true, published: true, attributedTo: true, inReplyTo: true, replies: true}, sort: {published: -1}, limit: limit});
    else
        return Posts.find({}, {fields: {id: true, published: true, attributedTo: true, inReplyTo: true, replies: true}, sort: {published: -1}, limit: limit});
});

Meteor.publish('abstractPostsByUser', function(actorID, limit = 1000, date = Date.now()) {
    limit = manageLimit(limit);
    if (date)
        return Posts.find({attributedTo: actorID, published:{ $lte: date}}, {fields: {id: true, published: true, attributedTo: true, inReplyTo: true, replies: true}, sort: {published: -1}, limit: limit});
    else
        return Posts.find({attributedTo: actorID}, {fields: {id: true, published: true, attributedTo: true, inReplyTo: true, replies: true}, sort: {published: -1}, limit: limit});
});

Meteor.publish('abstractPostsByTimeline', function(limit = 1000, date = Date.now()) {
    limit = manageLimit(limit);
    if (this.userId) {
        let actorID = Meteor.users.findOne({_id: this.userId}).actor;

        let actor = Actors.findOne({id: actorID}, {fields: {id: true, following: true}});

        let users = FollowingLists.findOne({id: actor.following}).orderedItems.concat([actor.id]);
        if (date)
            return Posts.find({attributedTo: {$in: users}, published:{ $lte: date}}, {fields: {id: true, published: true, attributedTo: true, inReplyTo: true, replies: true}, sort: {published: -true}, limit: limit});
        else
            return Posts.find({attributedTo: {$in: users}}, {fields: {id: true, published: true, attributedTo: true, inReplyTo: true, replies: true}, sort: {published: -true}, limit: limit});
    } else {
        return this.ready();
    }
});

//Universal subscription for roles.
Meteor.publish(null, function() {
    return Meteor.roles.find({});
});

//Returns info about the client user.
Meteor.publish('myself', function() {
    if (this.userId) {
        let actorID = Meteor.users.findOne({_id: this.userId}).actor;

        let actor = Actors.find({id: actorID});

        let actorData = actor.fetch()[0];

        //console.log(FollowerLists.findOne({id: actorData.followers}));

        return [Meteor.users.find({_id: this.userId}, {
            fields: {isBanned: true, seenPosts: true, actor: true}
        }), actor, FollowingLists.find({id: actorData.following}),
        FollowerLists.find({id: actorData.followers}),
        ActorLikedLists.find({id: actorData.liked})];
    } else {
        return this.ready();
    }
});

//Users; for the user management page. Should restrict fields even to moderators--shouldn't send user tokens and password hashes over network--ever.
Meteor.publish('users', function() {
    if (Roles.userIsInRole(this.userId, ['moderator'])) {
        return Meteor.users.find({}, {
            fields: {isBanned: true, createdAt: true, roles: true, emails: true}
        });
    } else {
        return Meteor.users.find({}, {
            fields: {isBanned: true, createdAt: true, roles: true}
        });
    }
});

//Actor data, for the profile page.
Meteor.publish('actor', function(actorID) {

    let actor = Actors.find({id: actorID});

    let actorData = actor.fetch()[0];

    let ret = [actor,
        FollowingLists.find({id: actorData.following}),
        FollowerLists.find({id: actorData.followers}),
        ActorLikedLists.find({id: actorData.liked})
    ];

    if (Meteor.user()) ret.push(Activities.find({type: "Follow", actor: Meteor.user().actor, object: actorID}));

    return ret;
});

//Actor data, for the profile page.
Meteor.publish('actorByHandle', function(handle) {
    return Actors.find({preferredUsername: handle});
});

Meteor.publish('followers', function(followersID) {
    return FollowerLists.find({id: followersID});
});

Meteor.publish('following', function(followingID) {
    return FollowingLists.find({id: followingID});
});

Meteor.publish('searchTerm', function(searchTerm) {

    let regex = new RegExp(".*" + searchTerm + ".*", 'i');

    let actors = Actors.find({$or: [{id: searchTerm}, {preferredUsername: regex}]}, {limit: 10});

    let posts = Posts.find({$or: [{id: searchTerm}, {summary: regex}]}, {limit: 10});

    return[actors, posts];
});
