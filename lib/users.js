/*
    Agora Forum Software
    Copyright (C) 2016 Gregory Sartucci
    License: AGPL-3.0, Check file LICENSE
*/
if(Meteor.isServer) {

    let checkModerator = function() {
        var loggedInUser = Meteor.user();

        if (!loggedInUser || !Roles.userIsInRole(loggedInUser, ['moderator'])) {
            throw new Meteor.Error(403, "Access denied");
        }
        return true;
    }

    Meteor.methods({
        switchBanned: function (targetUserId, isBanned) {
            checkModerator();

            Meteor.users.update(targetUserId, {
                $set: {isBanned: isBanned}
            });

            if (isBanned) {
                //Immediately logout user
                Meteor.users.update({_id: targetUserId}, {$set : { "services.resume.loginTokens" : [] }})
            }
        },

        switchModerator: function (targetUserId, isModerator) {
            checkModerator();

            if (isModerator) {
                Roles.addUsersToRoles(targetUserId, ['moderator']);
            } else {
                Roles.removeUsersFromRoles(targetUserId, ['moderator']);
            }
        },

        userExists: function(preferredUsername) {
            return Meteor.users.findOne({username: preferredUsername}) ? true : false;
        }
    })

    Meteor.users.before.insert(function(userId, user) {

        if (!user.actor) {

            let id = process.env.ROOT_URL + "actor/" + user.username;
            let actor = {
                preferredUsername: user.username,
                name: user.username,
                type: "Person",
                id: id,
                url: process.env.ROOT_URL + "@" + user.username,
                inbox: id + "/inbox",
                outbox: id + "/outbox",
                followers: id + "/followers",
                following: id + "/following",
                liked: id + "/liked",
                summary: "Empty user summary.",
                local: true
            };

            if (Actors.insert(actor)) user.actor = id;
        }

        user.seenPosts = [];
    });

    Meteor.users.after.insert(function(userId, user) {
        Accounts.sendVerificationEmail(this._id);
    });

    Meteor.setInterval(function() { //Remove posts that are older than 30 days from  a users list of seen posts. Those posts are automatically seen.

        Meteor.users.find({}).forEach(function(user) {
            if (user.seenPosts) {
                user.seenPosts.forEach(function(postID) {
                    let post = Posts.findOne({_id: postID});
                    if (Date.now() - post.published >= (1000*60*60*24*30))
                        Meteor.users.update({_id: user._id}, {$pull: {seenPosts: postID}});
                })
            }
        })

    }, 1000*60*60*24); //run function every day.

}
