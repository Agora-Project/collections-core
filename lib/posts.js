/*
    Agora Forum Software
    Copyright (C) 2016 Gregory Sartucci
    License: AGPL-3.0, Check file LICENSE
*/

POST_CONTENT_CHARACTER_LIMIT = 100000;
POST_SUMMARY_CHARACTER_LIMIT = 100;
POST_CONTENT_SUMMARY_REQUIREMENT = 500;

this.Posts = new Mongo.Collection('posts');

//TODO: Clean this up so that code for local post creation handling and code for fediverse import handling aren't tied together.
this.Posts.before.insert(function(userId, post) {

    if (post.id && Posts.findOne({id: post.id}))
        throw new Meteor.Error('Duplicate id!', 'Post with that id already exists!');

    if (post.local === undefined)
        throw new Meteor.Error('Post.local not set!', 'Post has been marked as neither local nor foreign!');

    //For local posts only, not fediverse ones:
    if (post.local) {
        let actor = Actors.findOne({id: post.attributedTo});

        //If the post doesn't have an URL, assign it one based on it's actor.
        if (actor) post.url = actor.url + "/" + post._id;

        //If the post doesn't have a fediverse id, assign it one based on it's local _id.
        if (!post.id) post.id = process.env.ROOT_URL + "post/" + post._id;

        //If the post doesn't have an array for replies, it gets an empty one.
        if (!post.replies) post.replies = post.id + "/replies";

        //If the post doesn't have an array for likes, it gets an empty one.
        if (!post.likes) post.likes = post.id + "/likes";

        //If the post doesn't have an array for shares, it gets an empty one.
        if (!post.shares) post.shares = post.id + "/shares";
    }
});

this.Posts.after.insert(function(userId, post) {
    if (post.inReplyTo) {
        let target = Posts.findOne({id: post.inReplyTo});
        if (!target) { //Is the post this replies to already present? If not,
            try {
                Meteor.call('importActivityJSONFromUrl', post.inReplyTo); //add it.
            } catch (error) {
                console.log(error, post.inReplyTo);
            }
        }

        if (post && post.local) {
            if (!ReplyLists.findOne({id: target.replies})) ReplyLists.insert({
                id: target.replies,
                type: "OrderedCollection",
                totalItems: 0,
                orderedItems: []
            });

            ReplyLists.update({id: target.replies}, {$inc: {totalItems: 1}, $push: {orderedItems: post.id}});
        }
    }

    if (post.attributedTo && !Actors.findOne({id: post.attributedTo})) { //Is this posts actor already present? If not,
        Meteor.call('importActivityJSONFromUrl', post.attributedTo); //add it.
    }
});

if (Meteor.isServer) {
    Meteor.startup(function() {
        Posts._ensureIndex({id: 1});
        Posts._ensureIndex({published: 1});
        Posts._ensureIndex({inReplyTo: 1});
        Posts._ensureIndex({summary: 1});
    });
}
