this.FollowerLists = new Mongo.Collection('follower-lists');
this.FollowingLists = new Mongo.Collection('following-lists');
this.ActorLikedLists = new Mongo.Collection('liked-lists');

this.PendingFollows = new Mongo.Collection('pending-follows');

this.ShareLists = new Mongo.Collection('share-lists');
this.LikeLists = new Mongo.Collection('like-lists');
this.ReplyLists = new Mongo.Collection('reply-lists');

let validateOrderedCollection = function(orderedCollection, mongoCollection) {
    if (mongoCollection.findOne({id: orderedCollection.id}))
        throw new Meteor.Error('Duplicate OrderedCollection', 'Cannot insert OrderedCollection: OrderedCollection with that id already present.');
};

FollowerLists.before.insert(function(userId, orderedCollection) {

});

FollowingLists.before.insert(function(userId, orderedCollection) {

});

if (Meteor.isServer) {

    this.Inboxes = new Mongo.Collection('inboxes');
    this.Outboxes = new Mongo.Collection('outboxes');

    Inboxes.before.insert(function(userId, orderedCollection) {
        validateOrderedCollection(orderedCollection, Inboxes);
    });

    Outboxes.before.insert(function(userId, orderedCollection) {
        validateOrderedCollection(orderedCollection, Outboxes);
    });


    Meteor.startup(function() {
        Inboxes._ensureIndex({id: 1});
        Outboxes._ensureIndex({id: 1});
        FollowerLists._ensureIndex({id: 1});
        FollowingLists._ensureIndex({id: 1});
        PendingFollows._ensureIndex({object: 1, actor: 1});
    });
}
